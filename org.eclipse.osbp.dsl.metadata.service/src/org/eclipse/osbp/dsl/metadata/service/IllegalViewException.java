package org.eclipse.osbp.dsl.metadata.service;

public class IllegalViewException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalViewException() {
		super();
	}

	public IllegalViewException(String var1, Throwable var2, boolean var3, boolean var4) {
		super(var1, var2, var3, var4);
	}

	public IllegalViewException(String var1, Throwable var2) {
		super(var1, var2);
	}

	public IllegalViewException(String var1) {
		super(var1);
	}

	public IllegalViewException(Throwable var1) {
		super(var1);
	}

}
