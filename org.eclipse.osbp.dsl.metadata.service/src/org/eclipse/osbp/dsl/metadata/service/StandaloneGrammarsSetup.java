/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.dsl.metadata.service;

import org.eclipse.osbp.dsl.datatype.xtext.DatatypeGrammarBundleSpaceSetup;
import org.eclipse.osbp.dsl.dto.xtext.DtoGrammarBundleSpaceSetup;
import org.eclipse.osbp.dsl.entity.xtext.EntityGrammarBundleSpaceSetup;
import org.eclipse.osbp.ecview.dsl.UIGrammarBundleSpaceStandaloneSetup;
import org.eclipse.osbp.ecview.uisemantics.UISemanticsGrammarBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.action.ActionDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.authorizationdsl.AuthorizationDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.blip.BlipDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.chart.ChartDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.cubedsl.CubeDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.datainterchange.DataDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.datamartdsl.DatamartDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.dialogdsl.DialogDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.entitymock.EntityMockDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryDslBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.menu.MenuDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.messagedsl.MessageDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.organizationdsl.OrganizationDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.perspective.PerspectiveDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.reportdsl.ReportDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.signal.SignalDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.statemachine.StatemachineDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.table.TableDSLBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.topologydsl.TopologyDSLBundleSpaceStandaloneSetup;

/**
 * A helper class to setup the Xtext grammars properly.
 */
@SuppressWarnings("restriction")
public class StandaloneGrammarsSetup {

	/**
	 * Setup to prepare guice injectors for runtime.
	 */
	public static void setup() {
		ActionDSLBundleSpaceStandaloneSetup.doSetup();
		AuthorizationDSLBundleSpaceStandaloneSetup.doSetup();
		BlipDSLBundleSpaceStandaloneSetup.doSetup();
		ChartDSLBundleSpaceStandaloneSetup.doSetup();
		CubeDSLBundleSpaceStandaloneSetup.doSetup();
		DataDSLBundleSpaceStandaloneSetup.doSetup();
		DatatypeGrammarBundleSpaceSetup.doSetup();
		DatamartDSLBundleSpaceStandaloneSetup.doSetup();
		DialogDSLBundleSpaceStandaloneSetup.doSetup();
		UIGrammarBundleSpaceStandaloneSetup.doSetup();
		UISemanticsGrammarBundleSpaceStandaloneSetup.doSetup();
		DtoGrammarBundleSpaceSetup.doSetup();
		EntityGrammarBundleSpaceSetup.doSetup();
		EntityMockDSLBundleSpaceStandaloneSetup.doSetup();
		FunctionLibraryDslBundleSpaceStandaloneSetup.doSetup();
		MenuDSLBundleSpaceStandaloneSetup.doSetup();
		MessageDSLBundleSpaceStandaloneSetup.doSetup();
		OrganizationDSLBundleSpaceStandaloneSetup.doSetup();
		PerspectiveDSLBundleSpaceStandaloneSetup.doSetup();
		ReportDSLBundleSpaceStandaloneSetup.doSetup();
		SignalDSLBundleSpaceStandaloneSetup.doSetup();
		StatemachineDSLBundleSpaceStandaloneSetup.doSetup();
		TableDSLBundleSpaceStandaloneSetup.doSetup();
		TopologyDSLBundleSpaceStandaloneSetup.doSetup();
	}
}
